# Dictionary text files containing technical terms and proper nouns in Mathematics

1. `dict_en_GB` --- Proper nouns and terms in British English
2. `dict_en_US` --- Spelling variants of terms in American English
3. `dict_de_DE` --- Technical terms in German

> Words are arranged in lexicographic order

> Basically, only `dict_en_US` is maintained. The other languages may follow up on
> demand.

# Usage

## Local use with textidote

```bash
textidote --check en --dict dict_en_US.text example.tex > gsc.html
```

See also [here](https://github.com/sylvainhalle/textidote#using-a-dictionary)

If you use the CSC [tex-spell-check.sh](https://gitlab.mpi-magdeburg.mpg.de/software/helpers/-/blob/master/tex-spell-check.sh)

```bash
ln -s dict_en_US.text spell.en_US  # note the file name
tex-spell-check.sh en_US example.tex > gsc.html
```

## Use on the gitlab-CI

```yaml
tex-paper:
    image: gitlab.mpi-magdeburg.mpg.de/ci-images/minimal/latex:focal
    stage: deploy
    script:
        - update-csc-stuff
        - latexmk --pdf example.tex
        - tex-spell-check.sh en_US example.tex > gsc.html
    allow_failure: true
    artifacts:
        when: always
        paths:
            - example.pdf
            - gsc.html
```
